#!/bin/bash

UNAME=`uname`
if [ "$UNAME" != "Linux" ] ; then
    echo "Just only support Linux"
    exit 1
fi

DIR_PATH=$(dirname $(readlink -f $0))
if [ -z "$DIR_PATH" ] ; then
    echo "DIR_PATH can not empty"
    exit 1
fi

sudo rm -rf $DIR_PATH/data/core01/* \
            $DIR_PATH/data/core02/* \
            $DIR_PATH/data/core03/* \
            $DIR_PATH/log/core01/* \
            $DIR_PATH/log/core02/* \
            $DIR_PATH/log/core03/*

echo "Done!"
