#!/bin/bash

CONF_FILE="/etc/vchain/configure.conf"
source $CONF_FILE

CORE_DIR=$VC_CORE_DIR
LOG_DIR=$LOG_FOLDER_IN_CONTAINER
TIME=`date`
ENVIRONMENT="$1"
RULE="$2"

function change_setting_for_vchain_core {
    sed -i "s/^rpcuser=\"VC_CORE_RPCUSER\"/rpcuser=$VC_CORE_RPCUSER/g" \
            $CORE_DIR/gcoin.conf
    sed -i "s/rpcpassword=\"VC_CORE_RPCPASSWORD\"/rpcpassword=$VC_CORE_RPCPASSWORD/g" \
            $CORE_DIR/gcoin.conf
    sed -i "s/rpcport=\"VC_CORE_RPC_CONTAINER_PORT\"/rpcport=$VC_CORE_RPC_CONTAINER_PORT/g" \
            $CORE_DIR/gcoin.conf
    sed -i "s/txindex=\"VC_CORE_TXINDEX\"/txindex=$VC_CORE_TXINDEX/g" \
            $CORE_DIR/gcoin.conf
    sed -i "s/rpcallowip=\"VC_CORE_RPC_ALLOW_IP\"/rpcallowip=$VC_CORE_RPC_ALLOW_IP/g" \
            $CORE_DIR/gcoin.conf
    sed -i "s/^alliance=\"VC_CORE_ALLIANCE\"/alliance=$VC_CORE_ALLIANCE/g" \
            $CORE_DIR/gcoin.conf
    sed -i "s/^miner=\"VC_CORE_MINER\"/miner=$VC_CORE_MINER_ADDRESS/g" \
            $CORE_DIR/gcoin.conf
    if [ "$RULE" == "miner" ] ; then
        sed -i "s/port=\"VC_CORE_CONTAINER_PORT\"/port=$VC_CORE01_CONTAINER_PORT/g" \
            $CORE_DIR/gcoin.conf
        sed -i "/^addnode=/d" $CORE_DIR/gcoin.conf
        echo "addnode=$VC_CORE02_HOST:$VC_CORE02_CONTAINER_PORT" >> $CORE_DIR/gcoin.conf
        sed -i "/^gen=/d" $CORE_DIR/gcoin.conf
        echo "gen=1" >> $CORE_DIR/gcoin.conf
    elif [ "$RULE" == "alliance" ] ; then
        sed -i "s/port=\"VC_CORE_CONTAINER_PORT\"/port=$VC_CORE02_CONTAINER_PORT/g" \
            $CORE_DIR/gcoin.conf
    elif [ "$RULE" == "fullnode" ] ; then
        sed -i "s/port=\"VC_CORE_CONTAINER_PORT\"/port=$VC_CORE03_CONTAINER_PORT/g" \
            $CORE_DIR/gcoin.conf
        sed -i "/^addnode=/d" $CORE_DIR/gcoin.conf
        echo "addnode=$VC_CORE02_HOST:$VC_CORE02_CONTAINER_PORT" >> $CORE_DIR/gcoin.conf
    fi
}

if [ ! -d $CORE_DIR/main/blocks ] ; then
    change_setting_for_vchain_core
    echo "############## First time start at $TIME ##############" >> $LOG_DIR/core.log
    gcoind -daemon 1>>$LOG_DIR/core.log 2>>$LOG_DIR/core.err.log
    echo "Waitting gcoin daemon start" >> $LOG_DIR/core.log
    until gcoin-cli getinfo 1>/dev/null 2>&1 ; do
        printf "." >> $LOG_DIR/core.log
        sleep 1
    done
    echo "Gcoin daemon started. Start set generate" >> $LOG_DIR/core.log
    if [ "$RULE" == "miner" ] ; then
        TIME=`date`
        echo "#################### importprivkey ####################" >> $LOG_DIR/gcoin-cli.log
        gcoin-cli importprivkey $VC_CORE_MINER_PRIV_KEY >> $LOG_DIR/gcoin-cli.log
        TIME=`date`
        echo "################# assignfixedaddress ##################" >> $LOG_DIR/gcoin-cli.log
        gcoin-cli assignfixedaddress $VC_CORE_MINER_ADDRESS >> $LOG_DIR/gcoin-cli.log
    elif [ "$RULE" == "alliance" ] ; then
        TIME=`date`
        echo "#################### importprivkey ####################" >> $LOG_DIR/gcoin-cli.log
        gcoin-cli importprivkey $VC_CORE_ALLIANCE_PRIV_KEY >> $LOG_DIR/gcoin-cli.log
    fi
    sleep 3
    gcoin-cli stop 1>>$LOG_DIR/gcoin-cli.log 2>>$LOG_DIR/gcoin-cli.err.log
    sleep 1
else
    if [ "$ENVIRONMENT" == "-production" -o "$ENVIRONMENT" == "-p" ] ; then
        TIME=`date`
        if cat $CORE_DIR/gcoin.conf | grep VC_CORE_RPCUSER 1>/dev/null 2>&1; then
            change_setting_for_vchain_core
        fi
        echo "#################### Start at $TIME ####################" >> $LOG_DIR/core.log
        gcoind -daemon 1>>$LOG_DIR/core.log 2>>$LOG_DIR/core.err.log
        exec /sbin/init
    elif [ "$ENVIRONMENT" == "-modify" -o "$ENVIRONMENT" == "-m" ]; then
        change_setting_for_vchain_core
        exit 0
    else
        exec /sbin/init
    fi
fi
