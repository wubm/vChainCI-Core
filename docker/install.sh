#!/bin/bash

main() {
    UNAME=`uname`
    if [ "$UNAME" == "Darwin" ] ; then
        # Mac OS
        DIR_PATH=$(cd "$(dirname "$0")"; pwd)
    elif [ "$UNAME" == "Linux" ] ; then
        # Linux
        DIR_PATH=$(dirname $(readlink -f $0))
    fi
    if [ -z "$DIR_PATH" ] ; then
        echo "DIR_PATH can not empty"
        exit 1
    fi
    
    if [ "$UNAME" == "Linux" ] ; then
        if docker 1>/dev/null 2>&1 ; then
            docker -v
        else
            echo "Install docker"
            curl -fsSl https://get.docker.com | sh
            sudo usermod -aG docker $(whoami)
        fi
        
        if docker-compose -v 1>/dev/null 2>&1 ; then
            docker-compose -v
        else
            echo "Install docker-compose"
            sudo curl -L "https://github.com/docker/compose/releases/download/1.11.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
            sudo chmod +x /usr/local/bin/docker-compose
        fi
    elif [ "$UNAME" == "Darwin" ] ; then
        if docker 1>/dev/null 2>&1 ; then
            docker -v
        else
            echo "Please install docker: https://docs.docker.com/docker-for-mac/"
        fi
        if docker-compose -v 1>/dev/null 2>&1 ; then
            docker-compose -v
        else
            echo "Please install docker-compose: https://docs.docker.com/compose/install/"
        fi 
    fi

    create_folder $DIR_PATH/data/core01 \
                  $DIR_PATH/data/core02 \
                  $DIR_PATH/data/core03 \
                  $DIR_PATH/log/core01 \
                  $DIR_PATH/log/core02 \
                  $DIR_PATH/log/core03
    echo "Done!"
}

# Create folder
# usage: create_folder foler1 folder2 folder3...
#    ie: create_folder /folder/path/you/want /foler/path/you/want2
create_folder() {
    for var in "$@"
    do
        if [ ! -d $var ] ; then
            mkdir -p $var
        fi
    done
}

main "$@"
