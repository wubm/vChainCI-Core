# About This Repo
Start using Gcoin.

# Install
1. ```cd vChainCI-Core/docker```
2. ```bash install.sh```

# Start service
### Working dir
vChainCI-Core/docker

### Launch container
1. ```docker-compose up -d```

### Stop all container
1. ```docker-compose stop``` to stop container

### Stop&remove all container
1. ```docker-compose down``` to stop and remove container

### Stop(start/restart) specific container(core01, core02, core03)
1. ```docker-compose stop core01``` to stop core01 container
2. ```docker-compose start core02``` to start core02 container
3. ```docker-compose restart core03``` to restart core03 container

### Login specific container(core01, core02, core03)
1. ```docker exec -ti core01 bash``` to login core01 container

### Port mapping
| container_name            | service        | host port   | container port | Rule     |
| :------------------------ |:-------------- | :---------- | :------------- | :------- |
| core01                    | core           | 17786       | 7786           | miner    |
| core01                    | rpc            | 18345       | 18345          | miner    |
| core02                    | core           | 27786       | 7786           | alliance |
| core02                    | rpc            | 28345       | 18345          | alliance |
| core03                    | core           | 37786       | 7786           | fullnode |
| core03                    | rpc            | 38345       | 18345          | fullnode |

# Clear data and log
1. ```cd vChainCI-Core/docker```
2. ```docker-compose down```
3. ```bash clear.sh```

